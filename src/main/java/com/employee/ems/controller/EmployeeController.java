package com.employee.ems.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import com.employee.ems.exceptionHandler.IdNotFoundException;
import com.employee.ems.model.Employee;
import com.employee.ems.service.EmployeeService;

//@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class EmployeeController {

 @Autowired
 private EmployeeService employeeService ;
 
 
 
 @PostMapping("/ems")
 public Employee save(@RequestBody Employee employee) {
	 return this.employeeService.save(employee);
 }
 
 @GetMapping("/employees")
 public Set<Employee> fetchAll(){
	 return this.employeeService.fetchAllDetails();
 }
 
 @GetMapping("/ems/{empId}")
 public Employee findbyId(@PathVariable int empId) throws IdNotFoundException {
	return this.employeeService.findById(empId);
	 
 }
 
 @DeleteMapping("/ems/{empId}")
 public void deleteById(@PathVariable int empId) {
	 
	 this.employeeService.deleteById(empId) ;
 }
 
 @GetMapping("/ems/leavebal/{empId}")
 public int findbyLeavebalncebyId(@PathVariable int empId) throws IdNotFoundException{
	 Employee emp = this.employeeService.findById(empId);
	return emp.getLeaveBalance();
 }
 @GetMapping("/ems/manager/{empId}")
 public Set<Employee> listemployeeUnderManager(@PathVariable int empId) throws IdNotFoundException{
	 Employee emp = this.employeeService.findById(empId);
	  return  emp.getEmployeeeUnderManager();
	 
 }
 
 @GetMapping("/ems/name/{employeename}")
 public Employee employeeByEmpName(@PathVariable String employeename) {
	 return this.employeeService.findByEmployeename(employeename);
 }
 
 @GetMapping("/ems/listbybal/{leaveBal}")
 public List<Employee > findBYleaveBal(@PathVariable int leaveBal){
	 return this.employeeService.findByLeaveBal(leaveBal);
 }
 
}
