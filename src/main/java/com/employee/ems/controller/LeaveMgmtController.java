package com.employee.ems.controller;

import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.employee.ems.dao.EmployeeRepository;
import com.employee.ems.dao.LeaveRepository;
import com.employee.ems.exceptionHandler.IdNotFoundException;
import com.employee.ems.model.Employee;
import com.employee.ems.model.LeaveMgmt;
import com.employee.ems.service.EmployeeService;
import com.employee.ems.service.LeaveMgmtService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class LeaveMgmtController {
	
	@Autowired
	private LeaveMgmtService leaveservice;
	@Autowired
	private EmployeeService empservice;

	@PostMapping("/leave/{empId}")
	public LeaveMgmt applyForLeave(@PathVariable int empId,@RequestBody LeaveMgmt leaveDetails) throws IdNotFoundException {
		Employee emp = this.empservice.findById(empId);
		int numberOfDays = (int) ChronoUnit.DAYS.between(leaveDetails.getFromDate(),leaveDetails.getFromDate());
		emp.setLeaveBalance(emp.getLeaveBalance()-numberOfDays);
		leaveDetails.setEmployee(emp);
		return this.leaveservice.applyforLeave(leaveDetails);
	}
	
	
	@GetMapping("/getleavebyid/{leaveId}")
	public LeaveMgmt getLeaveId(@PathVariable int leaveId) throws IdNotFoundException{
		return this.leaveservice.getLeaveDetailsbyId(leaveId) ;
		
	}
	
	@GetMapping("/getleavebyempid/{empId}")
	public Set<LeaveMgmt> getLeaveByEmpId(@PathVariable int empId) throws IdNotFoundException{
        Employee emp = this.empservice.findById(empId);
       return  emp.getLeaveDetails();
		
	}
	
	@PostMapping("/update/{leaveId}")
	public LeaveMgmt update(@PathVariable int leaveId,int managerId, String reason ) throws IdNotFoundException {
		return this.leaveservice.update(leaveId, managerId, reason);
	}
	
}
