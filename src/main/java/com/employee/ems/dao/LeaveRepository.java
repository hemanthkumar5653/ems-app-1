package com.employee.ems.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.employee.ems.model.LeaveMgmt;

@Repository
public interface LeaveRepository extends JpaRepository<LeaveMgmt, Integer> {
	List<LeaveMgmt> findAll();
}
