package com.employee.ems.exceptionHandler;

import static org.springframework.http.HttpStatus.NOT_FOUND;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(NOT_FOUND)
public class IdNotFoundException extends Exception{
	
	public IdNotFoundException(String message) {
		super(message);
	}

	@Override
    public String getMessage() {
        return super.getMessage();
    }
	
}
