package com.employee.ems.model;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "employee")
@Getter
@Setter
@ToString

public class Employee {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "employee_id")
	@JsonProperty("employee_id")
	private int empId;

	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "managerId")
	@JsonBackReference
	private Employee managerId;

	@Column(name = "doj")
	@JsonProperty("doj")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yy")
	private LocalDate dateOfJoining;

	@Column(name = "emp_name", nullable = false)
	@JsonProperty("emp_name")
	private String employeename;

	@Column(name = "emp_mailId", nullable = true)
	@JsonProperty("emp_emailId")
	private String emailId;

	@Column(name = "leave_balance")
	@JsonProperty("leave_balance")
	private int leaveBalance;
	
	
	//@Column(name="emp_password")
	//private String password;

	@OneToMany(mappedBy = "managerId")
	@JsonIgnore
	private Set<Employee> employeeeUnderManager ;
	
	@OneToMany(mappedBy = "employee", cascade = CascadeType.ALL)
	@JsonIgnore
	private Set<LeaveMgmt> leaveDetails ;
	
  
}
