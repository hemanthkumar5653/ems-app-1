package com.employee.ems.model;

import java.time.LocalDate;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.*;

@Entity
@Table(name="leave_details")
@Getter
@Setter
@ToString
public class LeaveMgmt {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name ="leave_id")
	@JsonProperty("leave_id")
	private int leaveId;
	
	@Column(name="from_date")
	@JsonProperty("from_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING , pattern = "dd-MM-yy")
	private LocalDate fromDate;
	
	@Column(name="to_date")
	@JsonProperty("to_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING , pattern = "dd-MM-yy")
	private LocalDate toDate;
	
	@Column(name="reason")
	@JsonProperty("reason")
	private String reason;
	
	@ManyToOne
	@JoinColumn(name="employee_id")
	@JsonIgnore
	private Employee employee;
}
