package com.employee.ems.service;

import java.util.List;
import java.util.Set;

import com.employee.ems.exceptionHandler.IdNotFoundException;
import com.employee.ems.model.Employee;

public interface EmployeeService {

    public Employee save(Employee employee);
	
	public Set<Employee> fetchAllDetails();
	
	public Employee findById(int empId) throws IdNotFoundException;
	
	public void deleteById(int empId);
	
	public Employee findByEmployeename(String employeename);
	
	public List<Employee> findByLeaveBal(int leavebal);
	
}
