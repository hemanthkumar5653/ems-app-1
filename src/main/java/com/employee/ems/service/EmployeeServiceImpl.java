package com.employee.ems.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.employee.ems.exceptionHandler.IdNotFoundException;
import com.employee.ems.model.Employee;
import com.employee.ems.dao.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepo;

	@Override
	public Employee save(Employee employee) {
		return this.employeeRepo.save(employee);
	}

	@Override
	public Set<Employee> fetchAllDetails() {

		return new HashSet<>(this.employeeRepo.findAll());
	}

	@Override
	public Employee findById(int empId) throws IdNotFoundException {

		return this.employeeRepo.findById(empId).orElseThrow(() -> new IdNotFoundException("Invalid Employee Id"));
	}

	@Override
	public void deleteById(int empId) {
         this.employeeRepo.deleteById(empId);
	}

	@Override
	public Employee findByEmployeename(String employeename) {
		return this.employeeRepo.findByEmployeename(employeename);
	}

	@Override
	public List<Employee> findByLeaveBal(int leavebal) {
		return this.employeeRepo.findByLeaveBalance(leavebal);
	}


   

}
