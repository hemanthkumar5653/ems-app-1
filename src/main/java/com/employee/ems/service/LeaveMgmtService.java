package com.employee.ems.service;

import com.employee.ems.exceptionHandler.IdNotFoundException;
import com.employee.ems.model.LeaveMgmt;

public interface LeaveMgmtService {

     public LeaveMgmt applyforLeave( LeaveMgmt  leaveDetails);
	
	public LeaveMgmt update(int leaveId, int managerId, String reason) throws IdNotFoundException;
	
	public LeaveMgmt getLeaveDetailsbyId(int leaveId) throws IdNotFoundException;
	
	
}
