package com.employee.ems.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.employee.ems.dao.LeaveRepository;
import com.employee.ems.exceptionHandler.IdNotFoundException;
import com.employee.ems.model.Employee;
import com.employee.ems.model.LeaveMgmt;

@Service
public class LeaveMgmtServiceImpl implements LeaveMgmtService {

	@Autowired
	private LeaveRepository leaverepo;
	
	
	@Override
	public LeaveMgmt applyforLeave( LeaveMgmt leaveDetails) {
		return this.leaverepo.save(leaveDetails);
	}

	@Override
	public LeaveMgmt getLeaveDetailsbyId(int leaveId) throws IdNotFoundException {
		return this.leaverepo.findById(leaveId).orElseThrow(()-> new IdNotFoundException("id not found"));

	}

	@Override
	public LeaveMgmt update(int leaveId, int managerId, String reason) throws IdNotFoundException {
     LeaveMgmt leave= this.leaverepo.findById(leaveId).orElseThrow(()-> new IdNotFoundException("no details found"));
     Employee emp = leave.getEmployee();
     if(emp.getManagerId().getEmpId()== managerId) {
      leave.setReason(reason);
     }
	return leave;
	
	}

	

	
}
